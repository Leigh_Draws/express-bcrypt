
// Import des modules et config
const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const connection = require('./config/db');

// Utilisation du port via .env
const port = process.env.PORT;

// Config pour parser les requête avec un body en JSON et les requête encodées en URL
app.use(express.json());
app.use(express.urlencoded({
    extended: true,
}));

// Route qui affiche tous les utilisateurs
app.get('/', (req, res) => {
    connection.execute('SELECT * FROM user', (err, results) => {
        if (err) {
            res.status(500).send(err.message)
        } else {
            res.json(results)
        }
    })
});

// Route pour s'inscrire en temps que nouvel utilisateur
app.post('/register', (req, res) => {
    const { email, password } = req.body;

    // Si l'email ou le password ne sont pas renseigné, ça renvoit une erreur
    if (!email || !password) {
        res
            .status(400)
            .json({ error: 'Please specify both email and password' })
    } else {
        // Le mot de passe est hashé grâce à bcrypt et stocké dans la variable hashedPassword
        bcrypt.hash(password, 10, function (bcryptError, hashedPassword) {
            // Si bcrypt rencontre une erreur, le serveur la renvoie ici
            if (bcryptError) {
                res
                    .status(500)
                    .json({ error: bcryptError });
            } else {
                // Pas d'erreur, le serveur lance la requête SQL pour ajouter une ligne à la table user (avec le mot de passe hashé)
                connection.execute('INSERT INTO user (email, password) VALUES (?, ?)',
                    [email, hashedPassword],
                    (err, results) => {
                        if (err) {
                            res
                                .status(500)
                                .json({ error: err.message });
                        } else {
                            // Delete pour supprimer le mot de passe "visible" de l'objet de la requête
                            delete req.body.password;
                                const newUser = {
                                    ...req.body,
                                    user_id: results.insertId
                                };
                            res.status(201).json(newUser)
                        }
                    });
            }
        })
    }
});

// Route pour se connecter
app.post('/login', (req, res) => {
    const { email, password } = req.body;

    // Vérifie que les champs sont bien remplis
    if (!email || !password) {
        res
            .status(400)
            .json({ error: 'Please specify both email and password' })
    } else {
        // Vérifie que l'email correspond à une instance de la base de données
        connection.execute('SELECT * FROM user WHERE email = ?',
        [email],
        (mysqlError, results) => {
            if (mysqlError) {
              res.status(500).json({ error: mysqlError });

            // Si le résultat de la requête est un tableau vide, c'est qu'il n'y a pas d'instant ayant cet email dans la BDD
            } else if (results.length === 0) {
                res.status(401).json({ error: 'Invalid email'})
            } else {
                // On stocke l'objet dans une variable user pour accéder plus facilement à password
                const user = results[0]
                const hashedPassword = user.password

                // Avec bcrypt on compare le mot de passe rentré par l'utilisateur avec le mot de passe hashé en BDD
                bcrypt.compare(password, hashedPassword, function (bcryptError, passwordMatch) {
                    if (bcryptError) {
                        res.status(500).json({ error: bcryptError})
                    } else if (passwordMatch) {
                        // passwordMatch = true 
                        res.status(200).json({
                            id: user.id,
                            email: user.email
                        })
                    } else {
                        // Le mot de passe ne match pas
                        res.status(401).json({ error: 'Invalid Password'})
                    }
                })
            }
        })
    }
})

app.listen(port, () => {
    console.log(`Server launched on port http://localhost:${port}`)
})
